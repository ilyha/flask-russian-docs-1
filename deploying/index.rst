.. _deployment:

Варианты развёртывания
======================

В зависимости от того, что вы имеете в наличии, есть несколько
вариантов запуска приложений Flask. Во время разработки вы можете
использовать встроенный сервер, но для развёртывания приложений
для промышленного использования необходимо использовать вариант
полноценного развёртывания (не используйте в качестве "боевого"
встроенный сервер для разработки). Несколько вариантов развёртывания
документированы в данном разделе.

Если у вас другой WSGI сервер, чтобы понять, как с ним использовать
WSGI-приложения, читайте его документацию.  Просто не забывайте,
что фактически объект приложения :class:`Flask` и есть
WSGI-приложение.

Для того, чтобы быстро взять и применить приведённые варианты,
читайте :ref:`quickstart_deployment` в разделе Быстрый старт.

.. toctree::
   :maxdepth: 2

   mod_wsgi
   wsgi-standalone
   uwsgi
   fastcgi
   cgi

`Оригинал этой страницы <http://flask.pocoo.org/docs/deploying/>`_