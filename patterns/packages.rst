.. _larger-applications:

Большие приложения во Flask
===========================

В больших приложениях лучше использовать пакеты вместо модулей.  Это
очень просто.  Представьте, что небольшое приложение выглядит так::

    /yourapplication
        /yourapplication.py
        /static
            /style.css
        /templates
            layout.html
            index.html
            login.html
            ...

Простые пакеты
--------------

Чтобы преобразовать его в чуть большее, просто создайте новый каталог
приложения `yourapplication` внутри существующего и поместите всё в него.
Переименуйте `yourapplication.py` в `__init__.py`.  (Сначала убедитесь,
что удалили все файлы `.pyc`, иначе скорее всего оно перестанет работать).

У вас должно получиться что-то такое::

    /yourapplication
        /yourapplication
            /__init__.py
            /static
                /style.css
            /templates
                layout.html
                index.html
                login.html
                ...

Но как теперь запустить приложение?  Простой запуск ``python
yourapplication/__init__.py`` не сработает.  Скажем так, Python не хочет
запускать модули в пакете, как программы.  Но это не проблема, просто
добавим во внутренний каталог `yourapplication` новый файл `runserver.py`
со следующим содержимым::

    from yourapplication import app
    app.run(debug=True)

Что это нам даст?  Теперь мы можем реструктурировать приложение и поделить
его на несколько модулей.  Единственное, о чём нужно помнить и соблюдать,
это:

1. Создание приложения Flask должно происходить в файле `__init__.py`.  Так
   каждый модуль сможет безопасно импортировать его, а переменная `__name__`
   примет значение имени соответствующего пакета.
2. Все функции представлений, к которым применён декоратор
   :meth:`~flask.Flask.route`, должны быть импортированы в файл
   `__init__.py`.  Не сам объект, но модуль с ними.  Импорт модуля
   представлений производится **после создания объекта**.

Вот пример `__init__.py`::

    from flask import Flask
    app = Flask(__name__)

    import yourapplication.views

При этом `views.py` должен выглядеть так::

    from yourapplication import app

    @app.route('/')
    def index():
        return 'Hello World!'

В итоге должно получиться что-то вроде этого::

    /yourapplication
        /runserver.py
        /yourapplication
            /__init__.py
            /views.py
            /static
                /style.css
            /templates
                layout.html
                index.html
                login.html
                ...

.. admonition:: Взаимный импорт

   Каждый Python-программист его ненавидит, но мы его добавили: два модуля
   зависят друг от друга.  В данном случае `views.py` зависит от
   __init__.py.  Мы предостерегаем вас от подобного приёма, но здесь он
   оправдан.  Причина заключается в том, что мы на самом деле не используем
   представления в `__init__.py`, а просто убеждаемся в том, что модуль
   импортирован и делаем это в конце файла.

   Однако, у этого подхода всё-же есть проблемы, но если вы хотите
   использовать декораторы, без него не обойтись.  Обратитесь к разделу
   :ref:`becomingbig` за идеями о том, как с этим справиться.


.. _working-with-modules:

Работа с Blueprint'ами
----------------------

Если у вас большое приложение, рекомендуется поделить его на небольшие
группы - так, чтобы каждая группа была реализована с помощью blueprint'ов.
Для плавного вникания в данную тему обратитесь к разделу :ref:`blueprints`
данной документации.

`Оригинал этой страницы <http://flask.pocoo.org/docs/patterns/packages/>`_