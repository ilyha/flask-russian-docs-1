.. _tutorial-templates:

Шаг 6: Шаблоны
==============

Теперь нам следует поработать над шаблонами.  Если мы будем сейчас
запрашивать URL, мы получим лишь исключения, означающие, что Flask не
может найти шаблоны.  Шаблоны используют синтаксис `Jinja2`_ и по
умолчанию имеют автоматическую обработку специальных последовательностей
(autoescaping).  Это означает, что если вы в шаблоне вы не пометили
значение с помощью :class:`~flask.Markup` или с помощью фильтра
``|safe``, Jinja2 гарантирует, что особые символы, например ``<`` или
``>``, будут заменены спецпоследовательностями их XML-эквивалентов.

Также мы используем наследование шаблонов, что делает возможным повторно
использовать макет веб-сайта на всех страницах.

Поместите в папку `templates` следующие шаблоны:

.. _Jinja2: http://jinja.pocoo.org/2/documentation/templates

layout.html
-----------

Этот шаблон содержит скелет HTML, заголовок и ссылку для входа на сайт
(или выхода, если пользователь уже вошёл).  Он также отображает всплывающие
сообщения, если они есть.  Блок ``{% block body %}`` может быть заменён в
дочернем шаблоне на блок с тем же именем (``body``).

Также из шаблона доступен словарь :class:`~flask.session`, и вы можете
его использовать для проверки - вошёл ли пользователь или нет.  Заметим,
что в Jinja вы можете иметь доступ к отсутствующим атрибутам и элементам
объектов / словарей, что делает следующий код работоспособным, даже при
отсутствии в сессии ключа ``'logged_in'``:

.. sourcecode:: html+jinja

    <!doctype html>
    <title>Flaskr</title>
    <link rel=stylesheet type=text/css href="{{ url_for('static', filename='style.css') }}">
    <div class=page>
      <h1>Flaskr</h1>
      <div class=metanav>
      {% if not session.logged_in %}
        <a href="{{ url_for('login') }}">log in</a>
      {% else %}
        <a href="{{ url_for('logout') }}">log out</a>
      {% endif %}
      </div>
      {% for message in get_flashed_messages() %}
        <div class=flash>{{ message }}</div>
      {% endfor %}
      {% block body %}{% endblock %}
    </div>

show_entries.html
-----------------

Этот шаблон расширяет вышеприведённый шаблон `layout.html` для отображения
сообщений.  Заметим, что цикл `for` итерирует сообщения которые мы передаём
внутрь шаблона с помощью функции :func:`~flask.render_template`.  Также мы
сообщаем форме, чтобы она предала данные вашей функции `add_entry`,
используя при этом `POST` в качестве метода `HTTP`:

.. sourcecode:: html+jinja

    {% extends "layout.html" %}
    {% block body %}
      {% if session.logged_in %}
        <form action="{{ url_for('add_entry') }}" method=post class=add-entry>
          <dl>
            <dt>Title:
            <dd><input type=text size=30 name=title>
            <dt>Text:
            <dd><textarea name=text rows=5 cols=40></textarea>
            <dd><input type=submit value=Share>
          </dl>
        </form>
      {% endif %}
      <ul class=entries>
      {% for entry in entries %}
        <li><h2>{{ entry.title }}</h2>{{ entry.text|safe }}
      {% else %}
        <li><em>Unbelievable.  No entries here so far</em>
      {% endfor %}
      </ul>
    {% endblock %}

login.html
----------

Наконец, шаблон для осуществления входа, который просто-напросто отображает
форму, позволяющую пользователю войти под своим логином:

.. sourcecode:: html+jinja

    {% extends "layout.html" %}
    {% block body %}
      <h2>Login</h2>
      {% if error %}<p class=error><strong>Error:</strong> {{ error }}{% endif %}
      <form action="{{ url_for('login') }}" method=post>
        <dl>
          <dt>Username:
          <dd><input type=text name=username>
          <dt>Password:
          <dd><input type=password name=password>
          <dd><input type=submit value=Login>
        </dl>
      </form>
    {% endblock %}

Продолжение: :ref:`tutorial-css`.

`Оригинал этой страницы <http://flask.pocoo.org/docs/tutorial/templates/>`_